from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json

from .models import Presentation, Status


def api_list_presentations(request, conference_id):
    """
    Lists the presentations titles and the link to the
    presentations for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentations titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentations, the name of its status, and
    the link to the presentations's information.

    {
        "presentations": [
            {
                "title": presentations's title,
                "status": presentations's status name
                "href": URL to the presentations,
            },
            ...
        ]
    }
    """
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "status" in content:
                status = Status.objects.get(abbreviation=content["status"])
                content["status"] = status
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid status abbreviation"},
                status=400,
            )
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
