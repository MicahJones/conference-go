import json
from django.http import JsonResponse
from common.json import ModelEncoder
from events.acls import get_current_weather, get_lat_lon, get_photo
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods



class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
        ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }  

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location"
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

    try:
        location = Location.objects.get(id=content["location"])
        content["location"] = location
        conference = Conference.objects.create(**content)
        return JsonResponse(
                            conference,
                            encoder=ConferenceDetailEncoder,
                            safe=False,
                        )

    except Location.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )






@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        location = conference.location
        lat, lon = get_lat_lon(location.city, location.state.abbreviation)
        weather = get_current_weather(lat, lon) if lat and lon else None

        response_data = {
            "weather": weather,
            "conference": {
                "name": conference.name,
                "starts": conference.starts,
                "ends": conference.ends,
                "description": conference.description,
                "created": conference.created,
                "updated": conference.updated,
                "max_presentations": conference.max_presentations,
                "max_attendees": conference.max_attendees,
                "location": {
                    "name": location.name,
                    "href": location.get_api_url(),
                }
            }
        }
        return JsonResponse(response_data, encoder=ConferenceDetailEncoder, safe=False)
    
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0 })
    
    else:  # PUT request
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(conference, encoder=ConferenceDetailEncoder, safe=False)
    

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content['state'])
            content['state'] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        
        # city = content["city"]
        # state_abbreviation = state.abbreviation
        # photo_data = get_photo(city, state_abbreviation)
        # if photo_data["picture_url"]:
            # content["picture_url"] = photo_data["picture_url"]

        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)

        location = Location.objects.create(**content)
        print("************************************")
        print(location)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )



@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else: 
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
  



  
    
